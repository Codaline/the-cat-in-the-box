<%@	taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@	taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html lang="uk">
<head>
    <title>Авторизація</title>
</head>
<body>
<div class="container pad-0">
    <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 pad-0">
        <div class="panel panel-info" >
            <div class="panel-heading" style="">
                <div class="panel-title">
                    The cat in the box | Sing in
                </div>
            </div>
                <form id="loginform" class="form-horizontal" role="form" method="post" action="j_spring_security_check">
                    <div style="margin-bottom: 25px" class="input-group">
                        <input id="login-username" type="text" class="form-control" name="j_username" value="" placeholder="Логін">
                    </div>
                        <input id="login-password" type="password" class="form-control" name="j_password" placeholder="Пароль">
                    <div class="input-group" style="width:100%;">
                        <div class="checkbox">
                            <div class="controls" style="display: inline;float:right; position: relative;top:-10px;">
                                <button id="signin-btn" class="btn btn-success">Вхід</button>
                                <a id="btn-fblogin" href="/register" class="btn btn-primary">Реєстрація</a>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 control">
                            <div  class="checkbox" style="border-top: 1px solid#888; padding-top:15px; font-size:85%" >
                                <label>
                                    Запам'ятати мене<input id="login-remember" type="checkbox" name="_spring_security_remember_me" value="true">
                                </label>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>