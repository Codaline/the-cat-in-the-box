<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">
    <title>The Cat In The Box</title>
    <link type="text/css" rel="stylesheet" href="/css/simple-social-css-1.0.0.css"/>
    <link type="text/css" rel="stylesheet" href="/css/users/12.css"/>
    <link type="text/css" rel="stylesheet" href="/css/jumbo.css"/>
    <link type="text/css" rel="stylesheet" href="/css/style.css">
    <link type="text/css" rel="stylesheet" href="/css/animate.css">
    <link type="text/css" rel="stylesheet" href="/css/datepicker3.css">
    <link type="text/css" rel="stylesheet" href="">
    <script type="application/javascript" src="/js/jquery-2.1.1.js"></script>
    <script type="application/javascript" src="/js/ejs_production.js"></script>
    <script type="application/javascript" src="/js/jquery.widget.js"></script>
    <script type="application/javascript" src="/js/jquery-ui.min.js"></script>
    <script type="application/javascript" src="/js/bootstrap.min.js"></script>
    <script type="application/javascript" src="/js/bootstrap-dialog.min.js"></script>
    <script type="application/javascript" src="/js/bootstrap-datepicker.js"></script>
    <script type="application/javascript" src="/js/script.js"></script>
</head>
<body id="new">
<span id="logo-head">
<div class="container">
    <nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <img src="/images/logo.png" class="animated fadeInLeft">
            <%--<button type="button" class="navbar-toggle collapsed mar-top-50" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar" style="font-size: 16px;">--%>
                <%--<span class="sr-only">Toggle navigation</span>--%>
                <%--<span class="icon-bar"></span>--%>
                <%--<span class="icon-bar"></span>--%>
                <%--<span class="icon-bar"></span>--%>
            <%--</button>--%>
        </div>

        <%--<div id="navbar" class="navbar-collapse collapse pull-right mar-top-50">--%>
            <%--<button class="btn btn-success shadow">SIGN UP</button>--%>
            <%--<span class="mar-left-20" style="font-size: 14px">or</span>--%>
            <%--<button class="btn btn-link" >LOGIN</button>--%>

        <%--</div>--%>
    </div>
</nav>

    <div class="col-md-8">
        <h1 class="animated fadeInLeft" style="font-family: Impact, 'Arial Narrow Bold';text-transform: uppercase;font-size: 70px">
            Present this surprise
        </h1>
        <h3 style="text-shadow: 1px 1px 10px rgba(0,0,0,0.4)">You newer know whats inside the box</h3>
        <button onclick="$('#logo-head').hide();$('#back-ground').show();$('#pick-amount').show();" class="btn btn-success btn-lg animated fadeInLeft" style="font-size: 19px; text-transform: uppercase">
            <img src="/images/Icon_present_button.png" class="mar-bottom-8"> Buy gift
        </button>
    </div>
</div>

<div class="col-md-12 mar-top-30 animated fadeInDown" style="background: #ffffff">
    <div class="container">
        <h2 style="text-transform: uppercase;font-size: 26px;text-align: center;color:#335981" class="mar-top-30 mar-bottom-20">Simple three steps to get the present</h2>
        <div class="col-md-4 text-center">
            <img src="/images/Icon_step_1.png">
            <p style="font-size: 16px;text-align: center;color:#335981">
                Chose how much you want spend
            </p>
        </div>
        <div class="col-md-4 text-center">
            <img src="/images/Icon_step_2.png">
            <p style="font-size: 16px;text-align: center;color:#335981">
                Give us a tip about your preferences
            </p>
        </div>
        <div class="col-md-4 text-center">
            <img src="/images/Icon_step_3.png">
            <p style="font-size: 16px;text-align: center;color:#335981">
                Pay and receive your surprise delivery
            </p>
        </div>
    </div>
</div>
</span>

<table id="back-ground" style="display:none;min-height: 100vh; background: rgba(110, 110, 110, 0.8);">
    <tr>
        <td style="width: 100%;min-height: 100vh">
            <span id="pick-amount" style="display: none;">
                <div class="col-md-12 animated fadeInDown">
                    <div class="row" style="position:relative;background: url('/images/back.png') left top fixed;">
                        <button type="button"
                                class="btn btn-lg btn-warning"
                                onclick="location.href='/'"
                                style="position: fixed;top: 15px;z-index: 10000;right: 15px; background: transparent;">Close &times;</button>
                        <div class="container">
                            <div class="row">
                                <h1 class="text-center mar-20">So, lets do this!</h1>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 animated fadeInUp">
                    <div class="row pad-ver-25" style="background: #fff;color:#335981">
                        <div class="container">
                            <div class="col-md-4 text-center">
                                <img src="/images/Icon_step_1.png" style="max-width: 100%" class="animated fadeInLeft">
                                <p class="animated fadeInLeft">Lets pick amount</p>
                            </div>
                            <div class="col-md-8 animated fadeInRight">
                                <div class="col-sm-4 item">
                                    <div class="widget style1 btn btn-success" onclick="selectAmount(0.00,5.00)">
                                        <div class="row vertical-align">
                                            <div class="col-xs-12 text-center">
                                                <h2 class="font-bold">$ 5</h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 item">
                                    <div class="widget style1 btn btn-success" onclick="selectAmount(5.01,10.00)">
                                        <div class="row vertical-align">
                                            <div class="col-xs-12 text-center">
                                                <h2 class="font-bold">$ 10</h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 item">
                                    <div class="widget style1 btn btn-success" onclick="selectAmount(10.01,20.00)">
                                        <div class="row vertical-align">
                                            <div class="col-xs-12 text-center">
                                                <h2 class="font-bold">$ 20</h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </span>

            <span id="user-data" style="display: none;">
    <div class="col-md-12 animated fadeInUp">
        <div class="row" style="position:relative;background: url('/images/back.png') left top fixed;">
            <button type="button"
                    class="btn btn-lg btn-warning"
                    onclick="location.href='/'"
                    style="position: fixed;top: 15px;z-index: 10000;right: 15px; background: transparent;">Close &times;</button>
            <button type="button"
                    onclick="$('#user-data').hide();$('#pick-amount').show();"
                    class="btn btn-lg btn-warning"
                    style="position: fixed;top: 15px;z-index: 10000;background: transparent;">&lt; Back</button>
            <div class="container">
                <div class="row">
                    <h1 class="text-center mar-20">Let us know something about you</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 animated fadeInDown">
        <div class="row pad-ver-25" style="background: #fff;color:#335981">
            <div class="container">
                <div class="col-md-4 text-center">
                    <img src="/images/Icon_step_2.png" style="max-width: 100%" class="animated fadeInLeft">
                    <p class="animated fadeInLeft">Give us a tip about your preferences</p>
                </div>
                <div class="col-md-8 animated fadeInRight">
                    Name and surname
                    <input type="text" id="contactName" purchase maxlength="63" placeholder="Your name and surname">
                    Address 1
                    <input type="text" id="address1" purchase maxlength="127" placeholder="Address 1">
                    Address 2
                    <input type="text" id="address2" purchase maxlength="127" placeholder="Address 2">
                    Postal code, city/state
                    <input type="text" id="generalInfo" purchase maxlength="127" placeholder="Postal code, city/state">
                    Phone number
                    <input type="tel" id="phoneNum" purchase maxlength="63" placeholder="Your phone number">
                    Your preferences
                    <input type="text" id="prefer" purchase maxlength="127" placeholder="Your preferences">
                </div>
                <div class="col-md-12 text-center">
                    <button purchase onclick="setUserInfo()">Continue</button>
                </div>
            </div>
        </div>
    </div>
</span>

            <span id="cal-data" style="display: none">
            <div class="col-md-12">
                <div class="row" style="position:relative;background: url('/images/back.png') left top fixed;">

                    <div class="container">
                        <div class="row animated fadeInUp">
                            <h1 class="text-center mar-20 animated fadeInDown">
                                Purchase date
                            </h1>
                            <div id="calendar"></div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-12">
                <div class="row" style="position:relative;background: url('/images/back.png') left top fixed;">
                    <div class="container">
                        <div class="row text-center mar-top-15 mar-bottom-30">
                            <button purchase-submit onclick="getBox()">Get your Cat in The Box</button>
                        </div>
                    </div>
                </div>
            </div>
            </span>

            <span id="results" style="display:none;">
            <div class="col-md-12 animated fadeInUp">
                <div class="row" style="position:relative;background: url('/images/back.png') left top fixed;">
                    <button type="button"
                            class="btn btn-lg btn-warning"
                            onclick="location.href='/'"
                            style="position: fixed;top: 15px;z-index: 10000;right: 15px; background: transparent;">Close &times;</button>
                    <button type="button"
                            onclick="$('#results').hide();$('#user-data').show();"
                            class="btn btn-lg btn-warning"
                            style="position: fixed;top: 15px;z-index: 10000;background: transparent;">&lt; Back</button>
                    <div class="container">
                        <div class="row">
                            <h1 class="text-center mar-20">We found for you..</h1>
                        </div>
                    </div>
                </div>
            </div>
                <div class="col-md-12" >
                    <div class="row pad-ver-25" style="background: #fff;color:#335981">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-4 text-center">
                                    <img class="animated fadeInLeft" src="/images/Icon_step_3.png" style="max-width: 100%">
                                    <p class="animated fadeInLeft">Select what you want</p>
                                </div>
                                <div class="col-md-8 animated fadeInRight" id="items-content"></div>
                            </div>
                            <div class="row pad-top-40">
                                <div class="col-md-4">
                                    <h1 class="text-center">To pay:</h1>
                                </div>
                                <div class="col-md-8">
                                    <div class="col-md-3">
                                        FEE
                                        <h3>
                                            <span class="label label-warning">$ <i id="fee-p">0.00</i></span>
                                        </h3>
                                    </div>
                                    <div class="col-md-3">
                                        Delivery
                                        <h3>
                                            <span class="label label-warning">$ <i id="del-p">0.00</i></span>
                                        </h3>
                                    </div>
                                    <div class="col-md-3">
                                        Original price
                                        <h3>
                                            <span class="label label-warning">$ <i id="price-p">0.00</i></span>
                                        </h3>
                                    </div>
                                    <div class="col-md-3">
                                        <b>Summary</b>
                                        <h2>
                                            <span class="label label-success">$ <b id="summ">0.00</b></span>
                                        </h2>
                                    </div>
                                </div>
                                <div class="col-md-12 mar-top-15 text-center">
                                    <button purchase onclick="purchase()">SUBMIT AND PAY</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </span>
        </td>
    </tr>
</table>

<div class="modal fade" id="success-op" tabindex="-1" role="dialog" aria-labelledby="aditArtLabel" aria-hidden="true" style="z-index: 20000;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Закрити</span></button>
                <h4 class="modal-title" id="myModalLabel">Operation successful</h4>
            </div>
            <div class="modal-body" style="padding: 20px 15px 30px;text-align: center">
                We have received your order and it will be completed within 1-2 days.<br>
                <img src="/images/logo.png" class="animated fadeInLeft"><br>
                Thank you for using <b>The Cat In The Box</b>
            </div>
            <div class="modal-footer">
                <button type="button" onclick="location.href='/'" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

</body>
</html>
