<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Registration</title>
</head>
<body>
<form action="/register/reg" method="post">
  <input type="text" name="firstName" placeholder="First name">
  <input type="text" name="lastName" placeholder="Last name">
  <input type="email" name="email" placeholder="E-mail">
  <input type="password" name="password" placeholder="Password">
  <button>Register me</button>
</form>
</body>
</html>
