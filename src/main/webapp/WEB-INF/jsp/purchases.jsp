<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">
    <title>The Cat In The Box</title>
    <link type="text/css" rel="stylesheet" href="/css/users/12.css"/>
    <link type="text/css" rel="stylesheet" href="/css/jumbo.css"/>
    <link type="text/css" rel="stylesheet" href="/css/style.css">
</head>
<body>
<div class="container">
    <div class="jumbotron">
        <h1 class="sm" id="flow-header">Lets pick amount</h1>

        <div class="row">
            <div class="col-md-4">
                <img src="/images/amount-icon.png" style="max-width: 100%">

                <p>Pick amount</p>
            </div>
            <div class="col-md-8">
                <input type="number" min="0" class="col-md-6" placeholder="from" maxlength="7" purchase/>
                <input type="number" min="0" class="col-md-6" placeholder="to" maxlength="8" purchase/>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <img src="/images/add-to-cart.png" style="max-width: 100%">

                <p>Select items</p>
            </div>
            <div class="col-md-8">
                <div class="col-sm-6 item">
                    <h2>Item name</h2>
                    <p>Some item description</p>
                    <p>
                        <ul class="nav nav-pills">
                            <li role="presentation" class="active" item>
                                <input id="ch1" type="checkbox">
                                <label for="ch1">Buy</label>
                            </li>
                            <li role="presentation"><span>$ 5.99</span></li>
                        </ul>
                    </p>
                </div>
                <div class="col-sm-6 item">
                    <h2>Item name</h2>
                    <p>Some item description</p>
                    <p>
                        <ul class="nav nav-pills">
                            <li role="presentation" class="active" item>
                                <input id="ch2" type="checkbox">
                                <label for="ch2">Buy</label>
                            </li>
                            <li role="presentation"><span>$ 5.99</span></li>
                        </ul>
                    </p>
                </div>
                <div class="col-sm-6 item">
                    <h2>Item name</h2>
                    <p>Some item description</p>
                    <p>
                        <ul class="nav nav-pills">
                            <li role="presentation" class="active" item>
                                <input id="ch3" type="checkbox">
                                <label for="ch3">Buy</label>
                            </li>
                            <li role="presentation"><span>$ 5.99</span></li>
                        </ul>
                    </p>
                </div>
                <div class="col-sm-6 item">
                    <h2>Item name</h2>
                    <p>Some item description</p>
                    <p>
                    <ul class="nav nav-pills">
                        <li role="presentation" class="active" item>
                            <input id="ch4" type="checkbox">
                            <label for="ch4">Buy</label>
                        </li>
                        <li role="presentation"><span>$ 5.99</span></li>
                    </ul>
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <img src="/images/pay.png" style="max-width: 100%">

                <p>Pay for it</p>
            </div>
            <div class="col-md-8">

            </div>
        </div>
    </div>

</div>
</body>

</html>
