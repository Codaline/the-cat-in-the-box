function initAuthScript(){
    $("#go-login").on("click",moderateLogin);
    $("#go-login-m").on("click",moderateLogin);
    $("#close-login").on("click", function(){
        $(document.body).css("position","relative");
        $('#login-wrap').css('top','-201vh');
        $('#close-login').hide();
    });

    $("#signin-btn").on("click", authorize);
}

function authorize(){
    var alert = $("#login-alert");
    var alertField = $("#alert-field");
    var emailEl = $("#login-name");
    var passEl = $("#login-password");
    var email = emailEl.val();
    var password = passEl.val();

    if(validateEmail(email)){
        if(password!=''){
            $.get("/login",{email:email,password:password},function(data){
                if(data.success){
                    alertField.show();
                    $("#login-success").show();
                    setTimeout(function(){location.href='/dashboard'},700);
                }else{
                    alertField.show();
                    passEl.val("");
                    alert.html('<b>'+data.message+'</b>');
                    alert.show(500);
                    hideBlock(alert, 5000);
                    hideBlock(alertField, 5500);
                }
            });
        }else{
            alertField.show();
            alert.html('<b>Введіть коректний пароль</b>');
            alert.show(500);
            hideBlock(alert, 5000);
            hideBlock(alertField, 5500);
        }
    }else{
        alertField.show();
        alert.html('<b>Введіть коректний email</b>');
        alert.show(500);
        hideBlock(alert, 5000);
        hideBlock(alertField, 5500);
    }
}


function initDashboard(){
    wait();

    var calendar = $('div#datepicker');

    calendar.datepicker({
        format: 'yyyy-mm-dd',
        todayBtn: 'linked',
        todayHighlight: true,
        clearBtn: false
    });
    var date = new Date();
    calendar.datepicker("update", date.toString("yyyy-mm-dd"));
    var dateFromPicker = calendar.datepicker("getDate").getTime();

    dateFromPicker = parseInt(dateFromPicker/1000);
    
    $(".addNew").on("click", showAddModal);
    $(".mButton").on("click", showSlideMenu);
    $(".hButton").on("click", hideSlideMenu);
    $("#sidebar-close").on("click", hideSlideMenu);
    
    updateRecords(false);
    getStatistics(dateFromPicker);
}

function showAddModal(){
    wait();
    $.get("/templates/addRecord.ejs", function(markup){
        $("#modalContainer").html(markup);
        $("#modalContent").modal('show');
        unwait();
    });
}

function hideSlideMenu(){
    $("#sidebar").css("left","-250vh");
    $("#sidebar-close").css("right","-100vh");
    $(".mButton").css("right","15px");
    $(".hButton").css('left','-300vh');
}

function showSlideMenu(){
    $(".mButton").css("right","-200vh");
    $("#sidebar").css("left","0");
    $(".hButton").css('left','0');
    $("#sidebar-close").css("right","0");
}

function updateRecords(hide){
    wait();
    $.get("/action",{act: "list"},function(data){
        var html = new EJS({url:'/templates/records'}).render({records:data});
        $("#records-list").html(html);
        if(hide) unwait();
    });
}

function getStatistics(date){
    $.get("/action",{act: "statistic",date:date},function(data){
        var html = new EJS({url:'/templates/statistic'}).render({data:data});
        $("#stats").html(html);
        unwait()
    });
}

function editRecord(id){
    wait();
    $.get("/action",{act: "get",id:id},function(data){
        var html = new EJS({url:'/templates/updateRecord'}).render({record:data});
        $("#modalContainer").html(html);
        $("#modalContent").modal('show');
        unwait()
    });
}

function sTo(element){
    $("html, body").animate({
        scrollTop: $("#" + element).offset().top - 60
    }, 1000);
}

function modalTo(element){
    $("html, body").animate({
        scrollTop: $("#" + element).offset().top
    }, 500);
}

function successAlertModal(message){
    $("#sMess").html(message);
    $("#successModal").modal('show');
}

function unsuccessAlertModal(message){
    $("#eMess").html(message);
    $("#errorModal").modal('show');
}

function moderateLogin(){
    $('#login-wrap').css('top','0');
    setTimeout(function(){$('#close-login').show();},1000);
    $(document.body).css("position","fixed");
}

function hideBlock(el, timeout){
    setTimeout(function(){
        el.hide(150)
    }, timeout)
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function formatDate(time){
    var date = new Date(time*1000);
    var monthes = ['січня','лютого','березня','квітня','травня','червня','липня','серпня','вересня','жовтня','листопада','грудня'];
    var month = monthes[date.getMonth()];
    var day = date.getDate();
    return day + " " + month + " " + date.getUTCFullYear();
}

function formatDateTime(time){
    var longDate = Date.parse(time);
    if(isNaN(longDate)){
        time = time.substr(0,10);
        console.log(time);
        return formatDate(new Date(time).getTime()/1000);
    }else {
        var date = new Date(time);
        var monthes = ['січня', 'лютого', 'березня', 'квітня', 'травня', 'червня', 'липня', 'серпня', 'вересня', 'жовтня', 'дистопада', 'грудня'];
        var month = monthes[date.getMonth()];
        var day = date.getDate();
        var hours = date.getHours() < 10 ? ("0" + date.getHours()) : (date.getHours());
        var minutes = date.getMinutes() < 10 ? ("0" + date.getMinutes()) : (date.getMinutes());
        return day + " " + month + " " + date.getUTCFullYear() + " " + hours + ":" + minutes;
    }
}

function showDeleteModal(deleteObject,id){
    wait();
    var html = new EJS({url: '/templates/delete'}).render({deleteObject:deleteObject});
    $("#modalContent").modal('hide');
    $("#modalContainer").append(html);
    $(".confirm").on("click",function(){
        deleteQuery(id)
    });
    $("#deleteModal").modal("show");
    unwait();
}

function deleteQuery(id){
    wait();
    $.get("/action",{act:"delete",id:id},function(callback){
        $("#deleteModal").modal('hide');
        if(callback.success) {
            successAlertModal(callback.message);
            updateRecords(false);
            if($(window).width()<768){
                hideSlideMenu();
            }
            sTo('records-list')
        }else {
            unsuccessAlertModal(callback.message);
        }
        unwait()
    });
}

function wait(){
    $("#waiting").show()
}

function unwait(){
    $("#waiting").hide()
}