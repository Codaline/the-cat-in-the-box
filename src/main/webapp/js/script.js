var items,selected = [],from,to;
function initDatepicker(){
    var $calendar = $("#calendar");
    $calendar.datepicker({
        format: "yyyy-mm-dd",
        todayBtn: "linked",
        todayHighlight: true,
        clearBtn: false
    });
    var date = new Date();
    $calendar.datepicker("update", date.toString("yyyy-mm-dd"));
    $calendar.css("padding-left",($calendar.width()-220)/2);
}

function selectAmount(f, t){
    $('#pick-amount').hide();
    from = f;
    to = t;
    $('#user-data').show()
}

function init(){
    initDatepicker();
}

function getItemById(id){
    var returnes;
    $(items).each(function (ind,el) {
        if (this.id == id) {
            returnes = this;
        }
    });
    return returnes;
}

function checkOut(){
    setInterval(function(){
        var checked = $("input[type=checkbox]:checked");
        if(checked.length==0){
            setPrices(0.00, 0.00, 0.00);
        }else{
            var fee = 0.0,
                delivery = 0.0,
                price = 0.0;
            checked.each(function(index){
                var item = getItemById($(this).attr("id"));
                fee = fee + item.fee;
                delivery = delivery + item.deliveryPrice;
                price = price + item.price;
            });
            setPrices(fee,delivery,price);
        }
    },1000);
}

function setPrices(fee, delivery, price) {
    $("#fee-p").html(fee.toFixed(2));
    $("#del-p").html(delivery.toFixed(2));
    $("#price-p").html(price.toFixed(2));
    $("#summ").html((fee + delivery + price).toFixed(2));
}

function getBox() {
    $.get("/purchase/get", {priceFrom: from, priceTo: to}, function (data) {
        items = JSON.parse(data);
        var html = new EJS({url:"/js/templates/items"}).render({items:items});
        $("#items-content").html(html);
    });
    //$("#cal-data").hide();
    $("#results").show();
    checkOut();
}

function isAllUserDetails() {
    return $("#contactName").val() != "" &&
        $("#address1").val() != "" &&
        $("#address2").val() != "" &&
        $("#generalInfo").val() != "" &&
        $("#phoneNum").val() != "" &&
        $("#prefer").val() != ""
}
function setUserInfo() {
    if (isAllUserDetails()) {
        $('#user-data').hide();
        getBox();
        //$('#cal-data').show();
        //initDatepicker();
    }
}

function purchase(){
    var contactName = $("#contactName").val(),
        address1 = $("#address1").val(),
        address2 = $("#address2").val(),
        generalInfo = $("#generalInfo").val(),
        phoneNum = $("#phoneNum").val(),
        prefer = $("#prefer").val();

    var checked = $("input[type=checkbox]:checked");
    if(checked.length==0){

    }else {
        checked.each(function(index){
            selected.push($(this).attr("id"));
        });
        var calendar = $('#calendar');
        var dateFromPicker = calendar.datepicker("getDate").getTime();
        if (isNaN(dateFromPicker)) {
            var date = new Date();
            dateFromPicker = calendar.datepicker("update", date.toString("yyyy-mm-dd"));
            dateFromPicker = calendar.datepicker("getDate").getTime();
        }
        $.post("/purchase/submit",
            {
                contactName: contactName,
                address1: address1,
                address2: address2,
                generalInfo: generalInfo,
                phoneNumber: phoneNum,
                date: dateFromPicker,
                message: prefer,
                items: JSON.stringify(selected)
            }, function (resp) {
                if (JSON.parse(resp)) {
                    $("#success-op").modal("show");
                }else{
                    alert("Request FAILED!")
                }
            });
    }
}

//$(document).ready(init);